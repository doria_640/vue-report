import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Bmap from '../views/Bmap'
import LiquidFill from '../views/LiquidFill'
import Pie from '../views/Pie'
// import Bmap2 from '../views/Bmap2'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/bmap',
    name: 'Bmap',
    component: Bmap
  },
  {
    path: '/liquid',
    name: 'Liquid',
    component: LiquidFill
  },
  {
    path: '/pie',
    name: 'Pie',
    component: Pie
  }
]

const router = new VueRouter({
  routes
})

export default router

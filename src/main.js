import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
import echarts from 'echarts' // echarts 4.x
// import * as echarts from 'echarts' // echarts 5.x
// import echarts from './utils/echartsUi' // 按需导入
import VueEcharts from 'vue-echarts'
import VeMap from 'v-charts/lib/map.common'
import VeBmap from 'v-charts/lib/bmap.common'
import VeLiquidFill from 'v-charts/lib/liquidfill.common'
import VeWordCloud from 'v-charts/lib/wordcloud.common'
import './assets/style/base.css'
import 'v-charts/lib/style.css'

Vue.config.productionTip = false
Vue.prototype.$echarts = echarts
Vue.component('vue-echarts', VueEcharts)
Vue.component('ve-map', VeMap)
Vue.component('ve-bmap', VeBmap)
Vue.component('ve-liquidfill', VeLiquidFill)
Vue.component('ve-wordcloud', VeWordCloud)
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')

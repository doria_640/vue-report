import request from '../utils/request'

export function wordcloud () {
  return request({
    url: '/screen/wordcloud',
    method: 'get'
    // params: {
    //   icode: '13926EAFCAA16CC3'
    // }
  })
}

export function mapScatter () {
  return request({
    url: '/screen/map/scatter',
    method: 'get'
    // params: {
    //   icode: '13926EAFCAA16CC3'
    // }
  })
}

export function screenData () {
  return request({
    url: '/screen/data',
    method: 'get'
    // params: {
    //   icode: '13926EAFCAA16CC3'
    // }
  })
}

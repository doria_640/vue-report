function format (v) {
  const reg = /\d{1,3}(?=(\d{3})+$)/g
  return `${v}`.replace(reg, '$&,')
}

function wrapperMoney (o, k) {
  return o && o[k] ? `￥ ${format(o[k])}` : '￥ 0'
}
function wrapperPercent (o, k) {
  return o && o[k] ? `${o[k]}%` : '0%'
}

function wrapperNumber (o, k) {
  return o && o[k] ? format(o[k]) : 0
}

function wrapperArray (o, k) {
  return o && o[k] ? o[k] : []
}

function wrapperValue (o, k) {
  return o && o[k] ? o[k] : 0
}

function wrapperObject (o, k) {
  if (o && k.indexOf('.') >= 0) {
    const keys = k.split('.')
    keys.forEach(key => {
      o = o[key]
    })
    return o
  } else {
    return o && o[k] ? o[k] : {}
  }
}
export default {
  computed: {
    reportData () {
      return this.getReportData()
    },
    salesToday () {
      return wrapperMoney(this.reportData, 'salesToday')
    },
    salesGrowthLastDay () {
      return wrapperPercent(this.reportData, 'salesGrowthLastDay')
    },
    salesGrowthLastMonth () {
      return wrapperPercent(this.reportData, 'salesGrowthLastMonth')
    },
    salesLastDay () {
      return wrapperMoney(this.reportData, 'salesLastDay')
    },
    orderToday () {
      return wrapperNumber(this.reportData, 'orderToday')
    },
    orderTrend () {
      return wrapperArray(this.reportData, 'orderTrend')
    },
    orderLastDay () {
      return wrapperNumber(this.reportData, 'orderLastDay')
    },
    userToday () {
      return wrapperNumber(this.reportData, 'userToday')
    },
    returnRate () {
      return wrapperPercent(this.reportData, 'returnRate')
    },
    userFullYear () {
      return wrapperArray(this.reportData, 'userFullYear')
    },
    userFullYearAxis () {
      return wrapperArray(this.reportData, 'userFullYearAxis')
    },
    userGrowthLastMonth () {
      return wrapperPercent(this.reportData, 'userGrowthLastMonth')
    },
    userGrowthLastDay () {
      return wrapperPercent(this.reportData, 'userGrowthLastDay')
    },
    userLastMonth () {
      return wrapperValue(this.reportData, 'userLastMonth')
    },
    userLastDay () {
      return wrapperValue(this.reportData, 'userLastDay')
    },
    userLastDayNumber () {
      return wrapperNumber(this.reportData, 'userLastDay')
    },
    userRank () {
      return wrapperArray(this.reportData, 'userRank')
    },
    orderFullYear () {
      return wrapperArray(this.reportData, 'orderFullYear')
    },
    orderFullYearAxis () {
      return wrapperArray(this.reportData, 'orderFullYearAxis')
    },
    orderRank () {
      return wrapperArray(this.reportData, 'orderRank')
    },
    category1 () {
      return wrapperObject(this.reportData, 'category.data1')
    },
    category2 () {
      return wrapperObject(this.reportData, 'category.data2')
    },
    category () {
      return wrapperObject(this.reportData, 'category')
    },
    wordcloud () {
      return this.getWordCloud()
    }
  },
  filters: {
    format (v) {
      return format(v)
    }
  },
  inject: ['getReportData', 'getWordCloud', 'getMapData']
}
